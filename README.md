# uc

Converts bytes to human readable.
Intended to be used with a wrapper that pulls from the selection buffer and
pushes a notification with the result.

## Compiling

cc -o uc uc.c

## Usage

./uc \<bytes to be converted>