#include <stdio.h>
#include <stdlib.h>

int uc(long double input, long double base, char *format)
{
	int i;

	if (input < base) {
		printf ("%.0Lf bytes\n", input);
		return 0;
	}

	char *u[] = {
		"K",
		"M",
		"G",
		"T",
		"P",
		"E",
		"Z",
		"Y",
	};

	input /= base;

	for (i = 0; input >= base && i < 7; i++, input /= base)
		;
	printf("%.2Lf %s%s\n", input, u[i], format);
	return 0;
}


int main(int argc, char *argv[])
{
	char *str = argv[1], *endptr;
	long double input = strtold(str, &endptr);

	uc(input, 1024, "iB");
	uc(input, 1000, "B");
}
